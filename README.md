GSVGtk is library to bring GTK+ widgets and Clutter Actors for SVG editing,
using GSVG as XML backend.

### Requires

<p>vala        >= 0.34.12, >= 0.36.7, 0.38.3</p>
<p>meson       >= 0.43</p>
<p>gxml-0.16   >= 0.16.1</p>
<p>gsvg-0.6    >= 0.5.1</p>
<p>librsvg-2.0 >= 2.40</p>

### Instalation

Clone repository using:

```git clone git@gitlab.com:pwmc/gsvgtk.git```

Then use meson build system:

```
$ mkdir _build
$ cd _build
$ meson .. --prefix=/usr/local
$ ninja
$ ninja install
```

prefix should be set to a directory of your choice, for installation.

Install may requires to get administrator privilages.

