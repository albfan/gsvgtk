/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvgtki-actor-rect-edit-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/rectangle/edit/move",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a shapes. Put mouse over");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh3 = svg.get_element_by_id ("rectangle");
              assert (sh3 != null);
              var c1 = new GSvgtk.ActorRectClutter.from_id (svg, "rectangle");
              s.add_child (c1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
