/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvgtki-stage-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/container/extract/resizable",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Resize Container","An stage with a circle actor");
        win.add_test ("Resize Container","Set X PPM to 2 pp/mm");
        win.add_test ("Resize Container","Set Y PPM to 2 pp/mm");
        win.add_test ("Resize Container","Set X PPM to 4 pp/mm");
        win.add_test ("Resize Container","Set Y PPM to 4 pp/mm");
        win.add_test ("Resize Container","Reset X PPM to 6 pp/mm");
        win.add_test ("Resize Container","Reset Y PPM to 6 pp/mm");
        win.add_test ("Resize Container","An stage with a set of actors: 4 lines, 1 circle");
        win.add_test ("Resize Container","Set X PPM to 2 pp/mm");
        win.add_test ("Resize Container","Set Y PPM to 2 pp/mm");
        win.add_test ("Resize Container","Set X PPM to 4 pp/mm");
        win.add_test ("Resize Container","Set Y PPM to 4 pp/mm");
        win.add_test ("Resize Container","Reset X PPM to 6 pp/mm");
        win.add_test ("Resize Container","Reset Y PPM to 6 pp/mm");
        win.add_test ("Resize Container","Set Width mm to 50 mm");
        win.add_test ("Resize Container","Set Height mm to 50 mm");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorCircle c1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var shc1 = svg.get_element_by_id ("circle");
              assert (shc1 != null);
              c1 = new GSvgtk.ActorCircleClutter.from_id (svg, "circle");
              s.add_child (c1 as Clutter.Actor);
            break;
            case 2:
              c1.x_ppm = 2.0;
            break;
            case 3:
              s.y_ppm = 2.0;
            break;
            case 4:
              s.x_ppm = 4.0;
            break;
            case 5:
              s.y_ppm = 4.0;
            break;
            case 6:
              s.x_ppm = 6.0;
            break;
            case 7:
              s.y_ppm = 6.0;
            break;
            case 8:
              var sh1 = svg.get_element_by_id ("line1");
              assert (sh1 != null);
              var l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
              s.add_child (l1);
              var sh2 = svg.get_element_by_id ("line2");
              assert (sh2 != null);
              var l2 = new GSvgtk.ActorLineClutter.from_id (svg, "line2");
              s.add_child (l2);
              var sh3 = svg.get_element_by_id ("line3");
              assert (sh3 != null);
              var l3 = new GSvgtk.ActorLineClutter.from_id (svg, "line3");
              s.add_child (l3);
              var sh4 = svg.get_element_by_id ("line4");
              assert (sh4 != null);
              var l4 = new GSvgtk.ActorLineClutter.from_id (svg, "line4");
              s.add_child (l4);
            break;
            case 9:
              c1.x_ppm = 2.0;
            break;
            case 10:
              s.y_ppm = 2.0;
            break;
            case 11:
              s.x_ppm = 4.0;
            break;
            case 12:
              s.y_ppm = 4.0;
            break;
            case 13:
              s.x_ppm = 6.0;
            break;
            case 14:
              s.y_ppm = 6.0;
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/add-shapes",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var w = new GtkClutter.Embed () {
          width_request = 800,
          height_request = 800 };
        var s = new GSvgtk.ActorContainer.with_parent (w.get_stage (), 100.0, 100.0);
        w.get_stage ().width = 800;
        w.get_stage ().height = 800;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        win.add_test ("Cotainer","Add image 0,0");
        win.add_test ("Cotainer","Add a line 0,0 50,50");
        win.add_test ("Cotainer","Add a rect 100,100,50,50");
        win.add_test ("Cotainer","Add a circle r=10");
        win.add_test ("Cotainer","Add a ellipse r=10");
        win.add_test ("Cotainer","Add a polyline zigzag");
        win.add_test ("Cotainer","Add a polygon 4 point star");
        win.add_test ("Cotainer","Add a \"Custom Text\" text");
        win.add_test ("Cotainer","Add a transformed polyline zigzag");
        win.add_test ("Cotainer","Add a transformed polygon zigzag");
        win.add_test ("Cotainer","Set X ppm to 5 pixels/mm");
        win.add_test ("Cotainer","Set Y ppm to 5 pixels/mm");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              s.add_svg (0,0, svg);
            break;
            case 2:
              s.add_line (0.0, 0.0, 50.0, 50.0);
              s.add_line (50.0, 50.0, 100.0, 0.0);
              s.add_line (50.0, 50.0, 0.0, 100.0);
              s.add_line (50.0, 50.0, 100.0, 100.0);
            break;
            case 3:
              s.add_rect (0.0, 0.0, 10.0, 10.0, 0.0, 0.0, null);
              s.add_rect (10.0, 10.0, 15.0, 15.0, 5.0, 5.0, null);
            break;
            case 4:
              s.add_circle (75.0, 25.0, 10.0);
              s.add_circle (75.0, 75.0, 10.0);
            break;
            case 5:
              s.add_ellipse (75.0, 50.0, 10.0, 20.0);
              s.add_ellipse (50.0, 75.0, 20.0, 10.0);
            break;
            case 6:
              s.add_polyline ("50,75 60,65 70,75 80,65", GSvg.Length.Type.MM, null);
            break;
            case 7:
              s.add_polygon ("50,75 46.5,81.5 40,85 46.5,88.5 50,95 53.5,88.5 60,85 53.5,81.5", GSvg.Length.Type.MM, null);
            break;
            case 8:
              s.add_text ("Custom Text", "Sans", "10mm", "5mm","95mm", null, null, null);
            break;
            case 9:
              s.add_polyline ("50,75 60,65 70,75 80,65", GSvg.Length.Type.UNKNOWN, "translate (-20,-20) scale(1.1)");
            break;
            case 10:
              s.add_polygon ("50,75 46.5,81.5 40,85 46.5,88.5 50,95 53.5,88.5 60,85 53.5,81.5", GSvg.Length.Type.MM, "translate (-50,-50) scale(0.5)");
            break;
            case 11:
              s.x_ppm = 5;
            break;
            case 12:
              s.y_ppm = 5;
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/extract/circle",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a circle actor");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorCircle c1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var shc1 = svg.get_element_by_id ("circle");
              assert (shc1 != null);
              c1 = new GSvgtk.ActorCircleClutter.from_id (svg, "circle");
              s.add_child (c1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/extract/line",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a line actor vertical");
        win.add_test ("Extract Shape Container","An stage with a line actor horizontal");
        win.add_test ("Extract Shape Container","An stage with a line actor horizontal");
        win.add_test ("Extract Shape Container","An stage with a line actor vertical");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh1 = svg.get_element_by_id ("line1");
              assert (sh1 != null);
              var l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
              s.add_child (l1);
            break;
            case 2:
              var sh2 = svg.get_element_by_id ("line2");
              assert (sh2 != null);
              var l2 = new GSvgtk.ActorLineClutter.from_id (svg, "line2");
              s.add_child (l2);
            break;
            case 3:
              var sh3 = svg.get_element_by_id ("line3");
              assert (sh3 != null);
              var l3= new GSvgtk.ActorLineClutter.from_id (svg, "line3");
              s.add_child (l3);
            break;
            case 4:
              var sh4 = svg.get_element_by_id ("line4");
              assert (sh4 != null);
              var l4 = new GSvgtk.ActorLineClutter.from_id (svg, "line4");
              s.add_child (l4);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/extract/ellipse",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a circle allipse");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorEllipse c1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var shc1 = svg.get_element_by_id ("ellipse");
              assert (shc1 != null);
              c1 = new GSvgtk.ActorEllipseClutter.from_id (svg, "ellipse");
              s.add_child (c1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/extract/text",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a text");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorText t1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh1 = svg.get_element_by_id ("text");
              assert (sh1 != null);
              t1 = new GSvgtk.ActorTextClutter.from_id (svg, "text");
              s.add_child (t1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/container/select",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a shapes. Put mouse over");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorText t1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh1 = svg.get_element_by_id ("text");
              assert (sh1 != null);
              t1 = new GSvgtk.ActorTextClutter.from_id (svg, "text");
              s.add_child (t1 as Clutter.Actor);
              var sh2 = svg.get_element_by_id ("ellipse");
              assert (sh2 != null);
              GSvgtk.ActorEllipse e1 = new GSvgtk.ActorEllipseClutter.from_id (svg, "ellipse");
              s.add_child (e1 as Clutter.Actor);
              var sh3 = svg.get_element_by_id ("line1");
              assert (sh3 != null);
              GSvgtk.ActorLine l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
              s.add_child (l1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
