/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;

class GSvgTest.Suite : Object
{
  static int main (string args[])
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Gtk.init (ref args);
    Test.add_func ("/gsvgtk/image/init",
    ()=>{
      var win = new Gtkt.WindowTester ();
      var w = new GSvgtk.Image ();
      win.widget = w;
      win.add_test ("Init","Showing an SVG");
      win.initialize.connect (()=>{
        try {
          var f = GLib.File.new_for_path ("tests/image-test-1.svg");
          message (f.get_path ());
          assert (f.query_exists ());
          var svg = new GSvg.GsDocument ();
          svg.read_from_file (f);
          w.svg = svg;
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = false; // Can be set to true if you don't window autoclose
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/image/width",
    ()=>{
      var win = new Gtkt.WindowTester ();
      var w = new GSvgtk.Image ();
      win.widget = w;
      win.add_test ("Init","Showing an SVG");
      win.initialize.connect (()=>{
        try {
          var f = GLib.File.new_for_path ("tests/image-test-1.svg");
          message (f.get_path ());
          assert (f.query_exists ());
          var svg = new GSvg.GsDocument ();
          svg.read_from_file (f);
          w.width = 100;
          w.svg = svg;
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = false; // Can be set to true if you don't window autoclose
      win.show_all ();
      Gtk.main ();
    });
    return Test.run ();
  }
}
