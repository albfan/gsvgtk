/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-actor-text-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/actor/text/load",
    ()=>{
      GSvg.Document svg = null;
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      win.add_test ("Init","Loading a Text: You should see just a text");
      win.initialize.connect (()=>{
        try {
          var f = GLib.File.new_for_path ("tests/shapes.svg");
          message (f.get_path ());
          assert (f.query_exists ());
          svg = new GSvg.GsDocument ();
          svg.read_from_file (f);
          var cr = svg.get_element_by_id ("text");
          assert (cr != null);
          message ((cr as GomElement).write_string ());
          var a = new GSvgtk.ActorTextClutter.from_id (svg, "text");
          w.get_stage ().add_child (a as Clutter.Actor);
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = false; // Can be set to true if you don't window autoclose
      win.show_all ();
      Gtk.main ();
    });
    return Test.run ();
  }
}
