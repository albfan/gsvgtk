/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvgtki-stage-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/container/edit/position",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainer ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a shapes. Put mouse over");
        var f = GLib.File.new_for_path ("tests/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        GSvgtk.ActorText t1 = null;
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh1 = svg.get_element_by_id ("ellipse");
              assert (sh1 != null);
              GSvgtk.ActorEllipse e1 = new GSvgtk.ActorEllipseClutter.from_id (svg, "ellipse");
              s.add_child (e1 as Clutter.Actor);
              var sh2 = svg.get_element_by_id ("rectangle");
              assert (sh2 != null);
              var r1 = new GSvgtk.ActorRectClutter.from_id (svg, "rectangle");
              s.add_child (r1 as Clutter.Actor);
              var sh3 = svg.get_element_by_id ("circle");
              assert (sh3 != null);
              var c1 = new GSvgtk.ActorCircleClutter.from_id (svg, "circle");
              s.add_child (c1 as Clutter.Actor);
              var sh4 = svg.get_element_by_id ("line1");
              assert (sh4 != null);
              GSvgtk.ActorLine l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
              s.add_child (l1 as Clutter.Actor);
              var sh5 = svg.get_element_by_id ("line2");
              assert (sh5 != null);
              GSvgtk.ActorLine l2 = new GSvgtk.ActorLineClutter.from_id (svg, "line2");
              s.add_child (l2 as Clutter.Actor);
              var sh6 = svg.get_element_by_id ("line3");
              assert (sh6 != null);
              GSvgtk.ActorLine l3 = new GSvgtk.ActorLineClutter.from_id (svg, "line3");
              s.add_child (l3 as Clutter.Actor);
              var sh7 = svg.get_element_by_id ("line4");
              assert (sh7 != null);
              GSvgtk.ActorLine l4 = new GSvgtk.ActorLineClutter.from_id (svg, "line4");
              s.add_child (l4 as Clutter.Actor);
              var sh8 = svg.get_element_by_id ("text");
              assert (sh8 != null);
              t1 = new GSvgtk.ActorTextClutter.from_id (svg, "text");
              s.add_child (t1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = true; // Can be set to true if you don't window autoclose
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
