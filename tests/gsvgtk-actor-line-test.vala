/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-actor-circle-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/actor/lines/load",
    ()=>{
      GSvg.Document svg = null;
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      win.add_test ("Line1","Loading a Line: You should see just a vertical line - center -> up");
      win.add_test ("Line2","Loading a Line: You should see just a vertical line - center -> left");
      win.add_test ("Line3","Loading a Line: You should see just a vertical line - center -> rigth");
      win.add_test ("Line4","Loading a Line: You should see just a vertical line - center -> down");
      var f = GLib.File.new_for_path ("tests/shapes.svg");
      message (f.get_path ());
      assert (f.query_exists ());
      svg = new GSvg.GsDocument ();
      try {
        svg.read_from_file (f);
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
      win.initialize.connect (()=>{
        try {
          switch (win.current_ntest) {
          case 1:
            var sh1 = svg.get_element_by_id ("line1");
            assert (sh1 != null);
            message ((sh1 as GomElement).write_string ());
            var l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
            w.get_stage ().add_child (l1);
          break;
          case 2:
            var sh2 = svg.get_element_by_id ("line2");
            assert (sh2 != null);
            message ((sh2 as GomElement).write_string ());
            var l2 = new GSvgtk.ActorLineClutter.from_id (svg, "line2");
            w.get_stage ().add_child (l2);
          break;
          case 3:
            var sh3 = svg.get_element_by_id ("line3");
            assert (sh3 != null);
            message ((sh3 as GomElement).write_string ());
            var l3 = new GSvgtk.ActorLineClutter.from_id (svg, "line3");
            w.get_stage ().add_child (l3);
          break;
          case 4:
            var sh4 = svg.get_element_by_id ("line4");
            assert (sh4 != null);
            message ((sh4 as GomElement).write_string ());
            var l3 = new GSvgtk.ActorLineClutter.from_id (svg, "line4");
            w.get_stage ().add_child (l3);
          break;
          }
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = true; // Can be set to true if you don't window autoclose
      win.show_all ();
      Gtk.main ();
    });
    return Test.run ();
  }
}
