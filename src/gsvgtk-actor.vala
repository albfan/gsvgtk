/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor.vala
 *
 * Copyright (C) 2017-2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Clutter;
using Cairo;

public interface GSvgtk.Actor : Object, GSvgtk.ActorResizable, GSvgtk.Positionable {

  // GSvgtk.Actor implementation
  public abstract GSvg.Document svg { get; set; }
  public abstract SvgRenderer renderer { get; set; }
  public abstract GSvgtk.DrawingArea drawing_area { get; }
  public abstract GSvgtk.Editor editor { get; internal set; }

  public virtual void set_svg_string (string str) throws GLib.Error {
    if (svg == null) svg = new GSvg.GsDocument ();
    svg.read_from_string (str);
    renderer.svg = svg;
  }

  public virtual bool draw_svg (Cairo.Context ctx, double width, double height) {
    try {
      if (renderer == null) return false;
      renderer.render_sized (ctx, width, height);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return true;
  }

  // Static methods
  public static double get_dpi (double width, GSvg.Length.Type units) {
    double m = 92;
    switch (units) {
      case GSvg.Length.Type.MM:
        m = width / 25.4;
        break;
      case GSvg.Length.Type.CM:
        m = width / 2.54;
        break;
    }
    return m;
  }

  /**
   * @param val Value to convert
   * @param type A GSvg.Length.Type to convert from
   * @param r Reference por GSvg.Length.Type.PERCENTAGE
   */
  public static float convert_svg_units_to_pixels (double val, GSvg.Length.Type type, double r = 1) {
    if (type == GSvg.Length.Type.UNKNOWN) return (float) val;
    Clutter.Units uc = Clutter.Units ();
    switch (type) {
      case  GSvg.Length.Type.PX:
      case  GSvg.Length.Type.NUMBER:
      case  GSvg.Length.Type.EXS:
      case  GSvg.Length.Type.PT:
      case  GSvg.Length.Type.PC:
        uc = Clutter.Units.from_pixels ((int) val);
        break;
      case  GSvg.Length.Type.EMS:
        uc = Clutter.Units.from_em ((float) val);
        break;
      case  GSvg.Length.Type.MM:
        uc = Clutter.Units.from_mm ((float) val);
        break;
      case  GSvg.Length.Type.CM:
        uc = Clutter.Units.from_cm ((float) val);
        break;
      case  GSvg.Length.Type.IN:
        uc = Clutter.Units.from_mm ((float) (val/25.4));
        break;
      case GSvg.Length.Type.PERCENTAGE:
        // FIXME: También se deben tomar en cuenta las unidades del padre de referencia
        uc = Clutter.Units.from_pixels ((int) (val * r));
        break;
    }
    uc.to_pixels ();
    return uc.get_unit_value ();
  }
}
