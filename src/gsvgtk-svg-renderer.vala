/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-svg-renderer.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 public interface GSvgtk.SvgRenderer : Object {
    public abstract GSvg.Document svg { get; set; }
    public abstract string id { get; set; }
    public abstract double x_ppm { get; set; }
    public abstract double y_ppm { get; set; }
    public abstract void render (Cairo.Context ctx) throws GLib.Error;
    public abstract void render_sized (Cairo.Context ctx, double width, double height) throws GLib.Error;
    public abstract Gdk.Pixbuf? render_pixbuf () throws GLib.Error;
    public abstract Gdk.Pixbuf? render_pixbuf_sized (double width, double height) throws GLib.Error;
 }
