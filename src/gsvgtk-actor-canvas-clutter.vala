/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-canvas-clutter.vala
 *
 * Copyright (C) 2017-2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Clutter;

public class GSvgtk.ActorCanvasClutter : Clutter.Canvas, GSvgtk.DrawingArea {
  private GSvgtk.Actor _parent;
  public GSvgtk.Actor parent { get { return _parent; } }
  construct {
    height = 400;
    width = 400;
  }
  public ActorCanvasClutter (GSvgtk.Actor parent) requires (parent is Clutter.Actor) {
    _parent = parent;
    ((Clutter.Actor) parent).set_content (this);
    draw.connect ((ctx, w, h)=>{ return draw_signal (ctx, w, h); });
  }
  public bool draw_signal (Cairo.Context ctx, double w, double h) {
    ctx.set_source_rgba (0, 0, 0, 0);
    ctx.rectangle (0, 0, w, h);
    ctx.fill ();
    return _parent.draw_svg (ctx, w, h);
  }
  public void GSvgtk.DrawingArea.invalidate () { (this as Clutter.Canvas).invalidate (); }
  public new void GSvgtk.DrawingArea.set_size (int width, int height) { (this as Clutter.Canvas).set_size (width, height); }
}
