/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-image.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;

[GtkTemplate (ui="/org/gnome/GSVGtk/gsvgtk-image.ui")]
public class GSvgtk.Image : Gtk.Grid {
  private GSvg.Document _svg = null;
  [GtkChild]
  private Gtk.Image image;

  public string str {
    owned get {
      try {
        return (svg as GomDocument).write_string ();
      } catch (GLib.Error e) {
        warning ("Error: "+e.message);
        return "";
      }
    }
    set {
      try {
        if (svg == null) svg = new GSvg.GsDocument ();
        (svg as GomDocument).read_from_string (str);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
  }
  public GSvg.Document svg {
    get {
      if (_svg == null) _svg = new GSvg.GsDocument ();
      return _svg;
    }
    set {
      _svg = value;
      try { render (); } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
  }
  /**
   * Width in pixels, to be used to define a scale factor for SVG to render
   *
   * A value of -1, means the size of the image will be rendered
   * at a default resolution of 96 DPI.
   */
  public int width {
    get { return image.pixel_size; }
    set { image.pixel_size = value; }
  }
  public Cairo.Surface surface { owned get { return image.surface; } }
  public void render () throws GLib.Error {
    if (_svg == null) return;
    var r = new SvgRendererRsvg ();
    r.svg = _svg;
    var img = r.render_pixbuf ();
    assert (img is Gdk.Pixbuf);
    image.pixbuf = img;
  }
}
