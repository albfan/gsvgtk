/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-shape.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Clutter;
using Cairo;

public interface GSvgtk.ActorShape : Object, GSvgtk.Actor {
  // Properties
  public abstract string id { get; internal set; }
  public abstract GSvg.Element element { get; internal set; }
  public abstract GSvg.Document fake_doc { get; internal set; }
  public abstract GSvg.Element mirror_element { get; internal set; }
  public abstract GSvg.Element mirror_gelement { get; internal set; }

  // Virtual/Abstract Methods
  /**
   * Use given SVG as a source and extract a {@link GSvg.Element},
   * with give id, to draw it.
   */
  public virtual void extract_element_from_id (GSvg.Document svg, string id) {
    this.id = id;
    try {
      var e = svg.get_element_by_id (id) as GSvg.Element;
      if (e == null) {
        warning ("Requested shape ID '%s' not found in SVG document. Using default shape".printf (id));
        return;
      }
      extract_element (e);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    drawing_area.invalidate ();
  }
  /**
   * Use given SVG as a source and extract given {@link GSvg.Element},
   * to draw it.
   */
  public virtual void extract_element (GSvg.Element element) {
    this.element = element;
    this.svg = element.owner_document as GSvg.Document;
    try {
      fake_doc = new GSvg.GsDocument ();
      var parent = fake_doc.create_svg (null, null, null, null, null, null, null);

      if (svg.root_element.x != null) {
        parent.x = new GsAnimatedLength ();
        parent.x.value = svg.root_element.x.value;
      }
      if (svg.root_element.y != null) {
        parent.y = new GsAnimatedLength ();
        parent.y.value = svg.root_element.y.value;
      }
      if (svg.root_element.width != null) {
        parent.width = new GsAnimatedLength ();
        parent.width.value = svg.root_element.width.value;
      }
      if (svg.root_element.height != null) {
        parent.height = new GsAnimatedLength ();
        parent.height.value = svg.root_element.height.value;
      }
      if (svg.root_element.content_script_type != null) parent.content_script_type = svg.root_element.content_script_type;
      if (svg.root_element.content_style_type != null) parent.content_style_type = svg.root_element.content_style_type;
      if (svg.root_element.viewport != null) {
        parent.viewport = new GSvg.GsRect ();
        parent.viewport.value = svg.root_element.viewport.value;
      }

      mirror_element = create_shape (parent, element);
      // Adding parents to get attributes from them
      GSvg.Element current = element;
      GSvg.Element p = current.parent_node as GSvg.Element;
      GSvg.Element np = null;
      GSvg.Element ce = mirror_element;
      while (p != null && !(p is GSvg.SVGElement)) {
        if (p is GSvg.GElement) {
          np = parent.create_g () as GSvg.GElement;
          np.append_child (ce);
          copy_attributes_element (p, np);
          current = p;
          p = current.parent_node as GSvg.Element;
          ce = np;
        }
      }
      mirror_gelement = parent.add_g ();
      if (np != null)
        mirror_gelement.append_child (np);
      else {
        copy_attributes_element (svg.root_element, parent);
        mirror_gelement.append_child (mirror_element);
      }
      fake_doc.append_child (parent);
      renderer.svg = fake_doc;
      double w = 400.0;
      double h = 400.0;
      get_shape_size (out w, out h);
      parent.width.base_val.value = w;
      parent.height.base_val.value = h;
      width_mm = w;
      height_mm = h;
      drawing_area.set_size ((int) (w * x_ppm), (int) (h * y_ppm));
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    drawing_area.invalidate ();
  }
  public virtual GSvg.Element create_shape (GSvg.SVGElement root, GSvg.Element shape) {
    if (shape is CircleElement) {
      string cx = "10mm";
      string cy = "10mm";
      string r = "8mm";
      if (shape is CircleElement) {
        var co = (CircleElement) shape;
        if (co.cx == null || co.cy == null || co.r == null) {
          warning ("No all attributes for circle has been set. Using defaults");
        } else {
          cx = co.cx.value;
          cy = co.cy.value;
          r = co.r.value;
        }
      }
      var c = root.create_circle (cx, cy, r);
      if (shape is CircleElement) {
        ActorShape.copy_attributes_element (shape, c);
      }
      if (c.stroke == null) c.stroke = "red";
      if (c.stroke_width == null) c.stroke_width = "1mm";
      return c;
    }
    if (shape is EllipseElement) {
      string cx = "10mm";
      string cy = "10mm";
      string rx = "8mm";
      string ry = "20mm";
      if (shape is EllipseElement) {
        var co = (EllipseElement) shape;
        if (co.cx == null || co.cy == null || co.rx == null || co.ry == null) {
          warning ("No all attributes for ellipse has been set. Using defaults");
        } else {
          cx = co.cx.value;
          cy = co.cy.value;
          rx = co.rx.value;
          ry = co.ry.value;
        }
      }
      var c = root.create_ellipse (cx, cy, rx, ry);
      if (shape is EllipseElement) {
        ActorShape.copy_attributes_element (shape, c);
      }
      if (c.stroke == null) c.stroke = "red";
      if (c.stroke_width == null) c.stroke_width = "1mm";
      return c;
    }
    if (shape is LineElement) {
      string x1 = "0mm";
      string y1 = "0mm";
      string x2 = "20mm";
      string y2 = "20mm";
      if (shape is LineElement) {
        var lo = (LineElement) shape;
        if (lo.x1 == null || lo.y1 == null || lo.x2 == null || lo.y2 == null) {
          warning ("No all attributes for the line has been set. Using defaults");
        } else {
          x1 = lo.x1.value;
          y1 = lo.y1.value;
          x2 = lo.x2.value;
          y2 = lo.y2.value;
        }
      }
      var l = root.create_line (x1, y1, x2, y2);
      if (shape is LineElement) {
        ActorShape.copy_attributes_element (shape, l);
      }
      if (l.stroke == null) l.stroke = "red";
      if (l.stroke_width == null) l.stroke_width = "3mm";
      return l;
    }
    if (shape is TextElement) {
      string xs = "10mm";
      string ys = "10mm";
      string txt = "";
      if (shape is TextElement) {
        var s = (TextElement) shape;
        if (s.x == null || s.y == null) {
          warning ("No all basic attributes for text has been set. Using defaults");
        } else {
          xs = s.x.value;
          ys = s.y.value;
        }
      }
      var nt = root.create_text (txt, xs, ys, null, null, null);
      if (shape is TextElement) {
        ActorShape.copy_attributes_element (shape, nt);
      }
      foreach (DomNode n in shape.child_nodes) {
        if (n is DomText) {
          nt.add_text ((n as DomText).data);
        }
        if (n is GSvg.TSpanElement) {
          var st = nt.create_empty_span ();
          try {
            (st as GomElement).read_from_string ((n as GomElement).write_string ());
            nt.append_child (st);
          } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
        }
        if (n is TRefElement) {
          nt.add_ref ((n as TRefElement).href.base_val);
        }
        if (n is TextPathElement) {
          var pt = nt.create_empty_path ();
          try {
            (pt as GomElement).read_from_string ((n as GomElement).write_string ());
            nt.append_child (pt);
          } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
        }
      }
      return nt;
    }
    if (shape is RectElement) {
      string x = "10mm";
      string y = "10mm";
      string width = "20mm";
      string height = "20mm";
      string rx = null;
      string ry = null;
      var r = (RectElement) shape;
      if (r.x == null || r.y == null || r.width == null || r.height == null) {
        warning ("No all attributes for rectangle has been set. Using defaults");
      } else {
        x = r.x.value;
        y = r.y.value;
        width = r.width.value;
        height = r.height.value;
        if (r.rx != null) {
          rx = r.rx.value;
        }
        if (r.ry != null) {
          ry = r.ry.value;
        }
      }
      var nr = root.create_rect (x, y, width, height, rx, ry, null);
      ActorShape.copy_attributes_element (shape, nr);
      if (nr.stroke == null) nr.stroke = "red";
      if (nr.stroke_width == null) nr.stroke_width = "1mm";
      return nr;
    }
    return shape;
  }

  public virtual void get_shape_size (out double w, out double h) {
    w = width_mm;
    h = height_mm;
    if (element is CircleElement) {
      if (element == null) return;
      if (!(element is CircleElement && mirror_element is CircleElement)) return;
      var c = (CircleElement) element;
      if (c.cx == null || c.cy == null || c.r == null) {
        warning ("One or more coordinates are missing");
        return;
      }
      if (c.r.base_val != null) {
        w = 2 * c.r.base_val.value;
        h = w;
      }
      var me = (CircleElement) mirror_element;
      me.cx.base_val.value = c.r.base_val.value;
      me.cy.base_val.value = c.r.base_val.value;
      var m = new GsAnimatedLength ();
      m.value = "0mm";
      if (c.stroke_width != null) {
        m.value = c.stroke_width;
        if (m.base_val != null) {
          w += m.base_val.value * 2;
          h += m.base_val.value * 2;
          me.cx.base_val.value += m.base_val.value;
          me.cy.base_val.value += m.base_val.value;
        }
      }
      x_mm = c.cx.base_val.value - c.r.base_val.value - m.base_val.value;
      y_mm = c.cy.base_val.value - c.r.base_val.value - m.base_val.value;
    }
    if (element is EllipseElement) {
      w = width_mm;
      h = height_mm;
      if (element == null) return;
      if (!(element is EllipseElement && mirror_element is EllipseElement)) {
        warning ("Referenced object type is not a: %s".printf (typeof (EllipseElement).name ()));
        return;
      }
      var c = (EllipseElement) element;
      if (c.cx == null || c.cy == null || c.rx == null || c.ry == null) {
        warning ("One or more coordinates are missing");
        return;
      }
      if (c.rx.base_val != null && c.ry.base_val != null) {
        w = 2 * c.rx.base_val.value;
        h = 2 * c.ry.base_val.value;
      }
      var me = (EllipseElement) mirror_element;
      me.cx.base_val.value = c.rx.base_val.value;
      me.cy.base_val.value = c.ry.base_val.value;
      var m = new GsAnimatedLength ();
      m.value = "0mm";
      if (c.stroke_width != null) {
        m.value = c.stroke_width;
        if (m.base_val != null) {
          w += m.base_val.value * 2;
          h += m.base_val.value * 2;
          me.cx.base_val.value += m.base_val.value;
          me.cy.base_val.value += m.base_val.value;
        }
      }
      x_mm = c.cx.base_val.value - c.rx.base_val.value - m.base_val.value;
      y_mm = c.cy.base_val.value - c.ry.base_val.value - m.base_val.value;
    }
    if (element is LineElement) {
      w = width_mm;
      h = height_mm;
      if (element == null) return;
      if (!(element is LineElement && mirror_element is LineElement)) return;
      var l = (LineElement) element;
      var ml = (LineElement) mirror_element;
      if (l.x1 == null || l.y1 == null || l.x2 == null || l.y2 == null) return;
      if (l.x1.base_val == null || l.y1.base_val == null || l.x2.base_val == null || l.y2.base_val == null) return;

      w = Math.fabs (l.x2.base_val.value - l.x1.base_val.value);
      h = Math.fabs (l.y2.base_val.value - l.y1.base_val.value);

      var lw = new GSvg.GsAnimatedLength ();
      if (l.stroke_width != null) { // FIXME: Add CSS checks
        lw.value = l.stroke_width;
        if (lw.base_val != null) {
          w += lw.base_val.value * 2;
          h += lw.base_val.value * 2;
        }
      }

      if (l.x2.base_val.value > l.x1.base_val.value) {
        ml.x2.base_val.value -= ml.x1.base_val.value - lw.base_val.value;
        ml.x1.base_val.value = lw.base_val.value;
        x_mm = l.x1.base_val.value;
      } else {
        ml.x1.base_val.value -= ml.x2.base_val.value - lw.base_val.value;
        ml.x2.base_val.value = lw.base_val.value;
        x_mm = l.x2.base_val.value;
      }

      if (l.y2.base_val.value > l.y1.base_val.value) {
        ml.y2.base_val.value -= ml.y1.base_val.value - lw.base_val.value;
        ml.y1.base_val.value = lw.base_val.value;
        y_mm = l.y1.base_val.value;
      } else {
        ml.y1.base_val.value -= ml.y2.base_val.value - lw.base_val.value;
        ml.y2.base_val.value = lw.base_val.value;
        y_mm = l.y2.base_val.value;
      }
      x_mm -= lw.base_val.value;
      y_mm -= lw.base_val.value;
    }
    if (element is TextElement) {
      w = width_mm;
      h = height_mm;
      if (element == null) return;
      if (!(mirror_element is TextElement)) {
        warning ("Mirror element is not a TextElement object");
        return;
      }
      var t = (TextElement) element;
      if (t.x == null || t.y == null ) {
        warning ("Text: One or more coordinates and properties are missing");
        return;
      }
      string font_family = "Sans";
      string text_anchor = "start";
      var m = new GsAnimatedLength ();
      m.value = "0mm";

      var p = t.parent_node as GSvg.Element;
      while (p != null) {
        if (p is GSvg.GElement || p is SVGElement) {
          if (p.font_size != null) {
            m.value = p.font_size;
          }
          if (p.font_family != null) {
            font_family = p.font_family;
          }
          if (p.text_anchor != null) {
            text_anchor = p.text_anchor;
          }
        }
        p = p.parent_node as GSvg.Element;
      }

      if (t.font_size != null) {
      // FIXME: Normalize units
        m.value = t.font_size;
      }

      if (t.font_family != null) {
        font_family = t.font_family;
      }

      // FIXME: Checkout CSS properties for values
      var slant = Cairo.FontSlant.NORMAL;
      var weight = Cairo.FontWeight.NORMAL;

      string txt = "";
      foreach (DomNode n in t.child_nodes) {
        if (n is DomText) {
          txt += ((DomText) n).data;
        }
      }
      var surf = new Cairo.ImageSurface (Format.ARGB32, 1024, 1024);
      Cairo.Context ct = new Cairo.Context (surf);
      ct.select_font_face (font_family, slant, weight);
      ct.set_font_size (m.base_val.value * y_ppm);
      TextExtents txe = TextExtents ();
      FontExtents fxe = FontExtents ();
      ct.text_extents (txt, out txe);
      ct.font_extents (out fxe);

      // FIXME: Add span nodes xs/ys pairs
      w = (txe.width + fxe.max_x_advance) / x_ppm;
      h = (txe.height + fxe.descent) / y_ppm;

      var tm = (TextElement) mirror_element;

      tm.x = new GsAnimatedLengthList ();
      tm.y = new GsAnimatedLengthList ();
      tm.y.value = "%gmm".printf ((txe.height - fxe.descent)/y_ppm);

      if (text_anchor != null) { // FIXME use top most svg's units
        switch (t.text_anchor) {
        case "end":
          tm.x.value = "%gmm".printf (w);
        break;
        case "middle":
          tm.x.value = "%gmm".printf (w/2);
        break;
        case "start":
        case "inherit":
        default:
          tm.x.value = "0mm";
        break;
        }
      } else {
          tm.x.value = "0mm";
      }
      try {
        double xp = 0.0;
        double yp = 0.0;
        if (t.x.base_val.number_of_items > 0) {
          xp = t.x.base_val.get_item (0).value - tm.x.base_val.get_item(0).value;
        }
        if (t.y.base_val.number_of_items > 0) {
          yp = t.y.base_val.get_item (0).value - tm.y.base_val.get_item(0).value;
        }
        x_mm = xp;
        y_mm = yp;
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    }
    if (element is RectElement) {
      w = width_mm;
      h = height_mm;
      if (element == null) return;
      if (!(element is RectElement && mirror_element is RectElement)) {
        warning ("Referenced object type is not a: %s".printf (typeof (RectElement).name ()));
        return;
      }
      var r = (RectElement) element;
      if (r.x == null || r.y == null || r.width == null || r.height == null) {
        warning ("One or more coordinates are missing");
        return;
      }
      if (r.width.base_val != null && r.height.base_val != null) {
        w = r.width.base_val.value;
        h = r.height.base_val.value;
      }
      var me = (RectElement) mirror_element;
      me.x.value = "0mm";
      me.y.value = "0mm";
      var m = new GsAnimatedLength ();
      m.value = "0mm";
      if (r.stroke_width != null) {
        m.value = r.stroke_width;
        w += m.base_val.value * 2;
        h += m.base_val.value * 2;
      }
      if (r.x.base_val != null && r.y.base_val != null) {
        x_mm = r.x.base_val.value - m.base_val.value;
        y_mm = r.y.base_val.value - m.base_val.value;
        me.x.base_val.value += m.base_val.value;
        me.y.base_val.value += m.base_val.value;
      } else {
        x_mm = 0;
        y_mm = 0;
      }
    }
  }

  // static methods
  public static void copy_attributes_element (GSvg.Element ori, GSvg.Element dst) {
    if (ori.id != null) dst.id = ori.id;
    if (ori.font != null) dst.font = ori.font;
    if (ori.font_family != null) dst.font_family = ori.font_family;
    if (ori.font_size != null) dst.font_size = ori.font_size;
    if (ori.font_size_adjust != null) dst.font_size_adjust = ori.font_size_adjust;
    if (ori.font_stretch != null) dst.font_stretch = ori.font_stretch;
    if (ori.font_style != null) dst.font_style = ori.font_style;
    if (ori.font_variant != null) dst.font_variant = ori.font_variant;
    if (ori.font_weight != null) dst.font_weight = ori.font_weight;
    if (ori.direction != null) dst.direction = ori.direction;
    if (ori.letter_spacing != null) dst.letter_spacing = ori.letter_spacing;
    if (ori.text_decoration != null) dst.text_decoration = ori.text_decoration;
    if (ori.unicode_bidi != null) dst.unicode_bidi = ori.unicode_bidi;
    if (ori.word_spacing != null) dst.word_spacing = ori.word_spacing;
    if (ori.clip != null) dst.clip = ori.clip;
    if (ori.color != null) dst.color = ori.color;
    if (ori.cursor != null) dst.cursor = ori.cursor;
    if (ori.display != null) dst.display = ori.display;
    if (ori.overflow != null) dst.overflow = ori.overflow;
    if (ori.visibility != null) dst.visibility = ori.visibility;
    if (ori.clip_path != null) dst.clip_path = ori.clip_path;
    if (ori.clip_rule != null) dst.clip_rule = ori.clip_rule;
    if (ori.mask != null) dst.mask = ori.mask;
    if (ori.opacity != null) dst.opacity = ori.opacity;
    if (ori.enable_background != null) dst.enable_background = ori.enable_background;
    if (ori.filter != null) dst.filter = ori.filter;
    if (ori.flood_color != null) dst.flood_color = ori.flood_color;
    if (ori.flood_opacity != null) dst.flood_opacity = ori.flood_opacity;
    if (ori.lighting_color != null) dst.lighting_color = ori.lighting_color;
    if (ori.stop_color != null) dst.stop_color = ori.stop_color;
    if (ori.stop_opacity != null) dst.stop_opacity = ori.stop_opacity;
    if (ori.pointer_events != null) dst.pointer_events = ori.pointer_events;
    if (ori.color_interpolation != null) dst.color_interpolation = ori.color_interpolation;
    if (ori.color_interpolation_filter != null) dst.color_interpolation_filter = ori.color_interpolation_filter;
    if (ori.color_profile != null) dst.color_profile = ori.color_profile;
    if (ori.color_rendering != null) dst.color_rendering = ori.color_rendering;
    if (ori.fill != null) dst.fill = ori.fill;
    if (ori.fill_opacity != null) dst.fill_opacity = ori.fill_opacity;
    if (ori.fill_rule != null) dst.fill_rule = ori.fill_rule;
    if (ori.image_rendering != null) dst.image_rendering = ori.image_rendering;
    if (ori.marker != null) dst.marker = ori.marker;
    if (ori.maker_end != null) dst.maker_end = ori.maker_end;
    if (ori.maker_mid != null) dst.maker_mid = ori.maker_mid;
    if (ori.maker_start != null) dst.maker_start = ori.maker_start;
    if (ori.shape_rendering != null) dst.shape_rendering = ori.shape_rendering;
    if (ori.stroke != null) dst.stroke = ori.stroke;
    if (ori.stroke_dasharray != null) dst.stroke_dasharray = ori.stroke_dasharray;
    if (ori.stroke_dashoffset != null) dst.stroke_dashoffset = ori.stroke_dashoffset;
    if (ori.stroke_linecap != null) dst.stroke_linecap = ori.stroke_linecap;
    if (ori.stroke_linejoin != null) dst.stroke_linejoin = ori.stroke_linejoin;
    if (ori.stroke_miterlimit != null) dst.stroke_miterlimit = ori.stroke_miterlimit;
    if (ori.stroke_opacity != null) dst.stroke_opacity = ori.stroke_opacity;
    if (ori.stroke_width != null) dst.stroke_width = ori.stroke_width;
    if (ori.text_rendering != null) dst.text_rendering = ori.text_rendering;
    if (ori.alignment_baseline != null) dst.alignment_baseline = ori.alignment_baseline;
    if (ori.baseline_shift != null) dst.baseline_shift = ori.baseline_shift;
    if (ori.dominant_baseline != null) dst.dominant_baseline = ori.dominant_baseline;
    if (ori.glyph_orientation_horizontal != null) dst.glyph_orientation_horizontal = ori.glyph_orientation_horizontal;
    if (ori.glyph_orientation_vertical != null) dst.glyph_orientation_vertical = ori.glyph_orientation_vertical;
    if (ori.kerning != null) dst.kerning = ori.kerning;
    if (ori.text_anchor != null) dst.text_anchor = ori.text_anchor;
    if (ori.writing_mode != null) dst.writing_mode = ori.writing_mode;
  }
}
