/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-drawing-area.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Clutter;

public interface GSvgtk.DrawingArea : Object {
  public abstract GSvgtk.Actor parent { get; }
  public abstract void invalidate ();
  public abstract void set_size (int width, int  height);

  public virtual bool draw_signal (Cairo.Context ctx, double w, double h) {
    ctx.set_source_rgba (0, 0, 0, 0);
    ctx.rectangle (0, 0, w, h);
    ctx.fill ();
    return parent.draw_svg (ctx, w, h);
  }
}
