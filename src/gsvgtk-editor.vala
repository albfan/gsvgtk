/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-editor.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public interface GSvgtk.Editor : Object {
  /**
   * A collection of changes, in order to provide undo capabilities.
   */
  public abstract GSvgtk.Changes changes { get; internal set; }
  public abstract GSvgtk.Actor actor  { get; internal set; }
  /**
   * Relative point list, using negative values when is moved in values with
   * lower value of the origin and positive if the are ahead.
   */
  public abstract GSvg.PointList movedto { get; internal set; }
  public abstract GSvg.Element modifications { get; internal set; }

  public virtual void save () {
    try {
      if (actor is ActorShape) {
        var shape = (ActorShape) actor;
        if (shape.element is GSvg.Element) {
          ActorShape.copy_attributes_element (modifications, shape.element);
        }
        if (shape.element is GSvg.Transformable && modifications is Transformable) {
          if ((modifications as Transformable).transform != null) {
            var tr = (Transformable) shape.element;
            tr.transform = new GsAnimatedTransformList ();
            tr.transform.value = (modifications as Transformable).transform.value;
          }
        }
        if (movedto == null) return;
        if (shape.element is CircleElement) {
          if (movedto.number_of_items == 0) return;
          var p = movedto.get_item (0);
          var c = (CircleElement) shape.element;
          c.cx = new GsAnimatedLengthCX ();
          c.cx.base_val.value = p.x;
          c.cy = new GsAnimatedLengthCY ();
          c.cy.base_val.value = p.y;
        }
        if (shape.element is EllipseElement) {
          if (movedto.number_of_items == 0) return;
          var p = movedto.get_item (0);
          var e = (EllipseElement) shape.element;
          e.cx = new GsAnimatedLengthCX ();
          e.cx.base_val.value = p.x;
          e.cy = new GsAnimatedLengthCY ();
          e.cy.base_val.value = p.y;
        }
        if (shape.element is TextElement) {
          var t = (TextElement) shape.element;
          if (t.x != null) {
            for (int i = 0; i < t.x.base_val.number_of_items; i++) {
              var x = t.x.base_val.get_item (i);
              if (i < movedto.number_of_items) {
                var p = movedto.get_item (i);
                x.value += p.x;
              }
            }
          }
          if (t.y != null) {
            for (int i = 0; i < t.y.base_val.number_of_items; i++) {
              var y = t.y.base_val.get_item (i);
              if (i < movedto.number_of_items) {
                var p = movedto.get_item (i);
                y.value += p.y;
              }
            }
          }
        }
        if (shape.element is LineElement) {
          var l = (LineElement) shape.element;
          if (l.x1 != null && l.y1 != null) {
            if (movedto.number_of_items > 0) {
              var p = movedto.get_item (0);
              l.x1.base_val.value += p.x;
              l.y1.base_val.value += p.y;
            }
          }
          if (l.x2 != null && l.y2 != null) {
            if (movedto.number_of_items > 1) {
              var p = movedto.get_item (1);
              l.x2.base_val.value += p.x;
              l.y2.base_val.value += p.y;
            }
          }
        }
        if (shape.element is RectElement) {
          var r = (RectElement) shape.element;
          if (movedto.number_of_items > 0) {
            var p = movedto.get_item (0);
            r.x.base_val.value += p.x;
            r.y.base_val.value += p.y;
          }
        }
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }
  public virtual void add_transformations (string transform) {
    if (!(modifications is Transformable)) modifications = new GSvg.GsTransformable ();
    if ((modifications as Transformable).transform == null) (modifications as Transformable).transform = new GsAnimatedTransformList ();
    try {
      var t = new GsAnimatedTransformList ();
      t.value = transform;
      for (int i = 0; i < t.base_val.number_of_items; i++) {
        var tr = t.base_val.get_item (i);
        (modifications as Transformable).transform.base_val.append_item (tr);
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }
  public virtual void set_css (string css) {
    if (modifications == null) modifications = new GSvg.GsTransformable ();
    var s = (Stylable) modifications;
    s.style = new GSvg.GsCSSStyleDeclaration ();
    s.style.value = css;
  }
  public virtual GSvg.Point current_point () {
    GSvg.Point p = new GsPoint ();
    try {
      if (actor is ActorShape)  {
        var shape = (ActorShape) actor;
        if (shape.element is CircleElement)  {
          var c = (CircleElement) shape.element;
          p.x = c.cx.base_val.value;
          p.y = c.cy.base_val.value;
        }
        if (shape.element is EllipseElement)  {
          var e = (EllipseElement) shape.element;
          p.x = e.cx.base_val.value;
          p.y = e.cy.base_val.value;
        }
        if (shape.element is TextElement)  {
          var t = (TextElement) shape.element;
          if (t.x == null || t.y == null) return p;
          if (t.x.base_val.number_of_items > 0) {
            p.x = t.x.base_val.get_item (0).value;
          }
          if (t.y.base_val.number_of_items > 0) {
            p.y = t.y.base_val.get_item (0).value;
          }
        }
        if (shape.element is RectElement)  {
          var r = (RectElement) shape.element;
          if (r.x == null || r.y == null) return p;
          p.x = r.x.base_val.value;
          p.y = r.y.base_val.value;
        }
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return p;
  }
  public abstract void undo ();
}
