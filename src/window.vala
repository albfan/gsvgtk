/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* window.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using GXml;
using GSvgtk;

namespace GSvgtk {
  [GtkTemplate (ui = "/org/gnome/GSVGtk/window.ui")]
  public class Window : Gtk.ApplicationWindow {
    private bool cont = false;
    private double rotate = 0.0;
    [GtkChild]
    Gtk.Image file_image;
    [GtkChild]
    Gtk.Image image;
    [GtkChild]
    Gtk.FileChooserButton sfile;
    [GtkChild]
    private Gtk.Box bximage;
    [GtkChild]
    private Gtk.Box bxactor;
    [GtkChild]
    private Gtk.ToggleButton tbanimate;
    [GtkChild]
    private Gtk.Stack stack;

    private GSvgtk.Image gimage;
    private GSvgtk.ActorImage aimage;

    construct {
      gimage = new GSvgtk.Image ();
      bximage.add (gimage);
      aimage = new GSvgtk.ActorImage ();
      var wactor = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400
      };
      wactor.get_stage ().x = 10;
      wactor.get_stage ().y = 10;
      wactor.get_stage ().width = 400;
      wactor.get_stage ().height = 400;
      wactor.get_stage ().add_child (aimage);
      bxactor.add (wactor);
      sfile.file_set.connect (()=>{
        try {
          GLib.File f = sfile.get_file ();
          var dsvg = new GSvg.GsDocument ();
          var svg = dsvg.add_svg (null, null, null,null,null);
          assert (svg != null);
          (svg as GomElement).read_from_file (f);
          if (stack.get_visible_child_name () == "image") {
            load_image (dsvg, f);
          }
        } catch (GLib.Error e) {
          warning ("Error: "+e.message);
        }
      });
      create_svg ();
      tbanimate.clicked.connect (()=>{
        if (tbanimate.active) {
          cont = true;
          Idle.add (animate);
        } else
          cont = false;
      });
    }

    private bool animate () {
      if (!cont) return Source.REMOVE;
      rotate += 1;
      if (rotate == 90.0) rotate = 0.0;
      try {
        var t = gimage.svg.get_element_by_id ("itext") as TSpanElement;
        if (t == null) {
          warning ("Element Not Found: itext");
          message ((gimage.svg as GomDocument).write_string ());
          return Source.CONTINUE;
        }
        t.font_size = "%0d".printf ((int)rotate);
        message ((gimage.svg as GomDocument).write_string ());
        gimage.render ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
      return Source.CONTINUE;
    }
    public void create_svg () {
      try {
        var dsvg = new GSvg.GsDocument ();
        var svg = dsvg.add_svg (null,null,"10cm","3cm","0 0 1000 300", null, "Example tspan01 - using tspan to change visual attributes");
        var g = svg.add_g ();
        g.font_family = "Verdana";
        g.font_size = "45";
        var t = svg.create_text (null,"200","150", "0", "0", "0");
        g.append_child (t);
        t.fill  = "blue";
        t.add_text ("You are ");
        t.id = "text";
        var ts = t.add_span ("not");
        ts.id = "itext";
        ts.font_weight = "bold";
        ts.fill = "red";
        t.add_text (" a banana");
        var r = svg.create_rect ("1","1","998","298", null, null, null);
        svg.append_child (r);
        r.fill = "none";
        r.stroke = "blue";
        r.stroke_width = "2";
        gimage.svg = dsvg;
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
    public void load_image (GSvg.Document dsvg, GLib.File f) throws GLib.Error {
      var rsvg = new Rsvg.Handle ();
      var str = new GLib.StringBuilder ();
      str.append ((dsvg as GomDocument).write_string ());
      rsvg.write (str.data);
      rsvg.close ();
      image.pixbuf = rsvg.get_pixbuf ();
      var frsvg = new Rsvg.Handle.from_file (f.get_path ());
      file_image.pixbuf = frsvg.get_pixbuf ();
    }

    public Window (Gtk.Application app) {
      Object (application: app);
    }
  }
}
