/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-container.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 using GXml;
 using GSvg;

public class GSvgtk.ActorContainer : Clutter.Actor, GSvgtk.Positionable, GSvgtk.ActorResizable, GSvgtk.Actor {
  private double _x_ppm;
  private double _y_ppm;
  private double _width_mm;
  private double _heigth_mm;
  private GSvg.Document main_doc;
  private GSvg.SVGElement main_element;
  private GSvg.GElement main_g;
  private GSvgtk.ActorClutter main_actor;
  private GSvgtk.Actor selected = null;
  private int svg_index = 0;
  private int line_index = 0;
  private bool dragging;
  private bool over_shape;
  // Resizable
  public double x_ppm {
    get { return main_actor.x_ppm; }
    set {
      main_actor.x_ppm = value;
      drawing_area.invalidate ();
    }
  }
  public double y_ppm {
    get { return main_actor.y_ppm; }
    set {
      main_actor.y_ppm = value;
      drawing_area.invalidate ();
    }
  }
  public double width_mm {
    get { return main_actor.width_mm; }
    set {
      main_actor.width_mm = value;
      drawing_area.invalidate ();
    }
  }
  public double height_mm {
    get { return main_actor.height_mm; }
    set {
      main_actor.height_mm = value;
      drawing_area.invalidate ();
    }
  }
  public float width_px { get { return main_actor.width; } }
  public float height_px { get { return main_actor.height; } }

  // GSvgtk.Actor
  public SvgRenderer renderer {
    get { return main_actor.renderer; }
    set { main_actor.renderer = value; }
  }
  public GSvg.Document svg {
    get { return main_doc; }
    set { main_doc = value; }
  }

  public GSvgtk.DrawingArea drawing_area { get { return main_actor.drawing_area; } }

  public GSvgtk.Editor editor { get; internal set; }


  // Positionable
  public double x_mm {
    get { return main_actor.x / x_ppm; }
    set { main_actor.x = (float) (value * x_ppm); }
  }
  public double y_mm {
    get { return main_actor.y / y_ppm; }
    set { main_actor.y = (float) (value * y_ppm); }
  }
  construct {
    set_background_color (Clutter.Color.from_string ("#0000f"));
    selected = null;
    dragging = false;
    over_shape = false;
    width = 600;
    height = 600;

    main_actor = new ActorImage ();
    main_actor.x_ppm = 4.0;
    main_actor.y_ppm = 4.0;
    main_actor.width_mm = 100.0;
    main_actor.height_mm = 100.0;

    _x_ppm = _y_ppm = 4.0;
    _width_mm = _heigth_mm = 100.0;
    main_doc = new GSvg.GsDocument ();
    main_element = main_doc.add_svg ("0mm", "0mm", "%fmm".printf (width_mm), "%fmm".printf (height_mm));
    main_element.fill = "black";
    main_g = main_element.add_g ();
    main_g.id = "main_g";

    main_actor.svg = main_doc;
    add_child (main_actor);

    parent_set.connect ((old)=>{
      var p = get_parent ();
      if (p == null) return;
      p.notify["width"].connect (()=>{
        var parent = get_parent ();
        if (parent != null) {
          width = p.width;
        }
      });
      p.notify["height"].connect (()=>{
        var parent = get_parent ();
        if (parent != null) {
          height = p.height;
        }
      });
      var pan = new Clutter.PanAction ();
      pan.name = "pan";
      p.add_action (pan);
      pan.acceleration_factor = 2;
      pan.pan.connect (()=>{
        if (!dragging && !over_shape) {
          float dx = 0.0f;
          float dy = 0.0f;
          pan.get_motion_delta (0, out dx, out dy);
          x += dx;
          y += dy;
          return true;
        }
        return false;
      });
    });

    actor_added.connect ((a)=>{
      if (!(a is GSvgtk.Actor)) return;
      var drag_action = new Clutter.DragAction ();
      drag_action.name = "drag-action";
      drag_action.drag_handle = a;
      a.add_action (drag_action);
      (a as GSvgtk.Actor).editor = new GSvgtk.EditorClutter (a as GSvgtk.Actor);
      a.enter_event.connect (()=>{
        if (selected != ((GSvgtk.Actor) a)) {
          a.background_color = Clutter.Color.from_string ("#888a85f1");
        }
        over_shape = true;
      });
      a.leave_event.connect (()=>{
        if (selected != ((GSvgtk.Actor) a)) {
          a.background_color = Clutter.Color.from_string ("#ffffff00");
        }
        over_shape = false;
      });
      a.button_press_event.connect ((e)=>{
        if (!(a is GSvgtk.Actor)) return false;
        if (selected == null) {
          selected = (GSvgtk.Actor) a;
          a.background_color = Clutter.Color.from_string ("#a40000f1");
          dragging = false;
        }
        if (selected != null && !dragging) {
          selected = null;
          a.background_color = Clutter.Color.from_string ("#ffffff00");
        }
        return true;
      });
      drag_action.drag_begin.connect ((a, x, y, mod)=>{
        dragging = true;
      });
      drag_action.drag_end.connect ((a, x, y, mod)=>{
        dragging = false;
      });
      drag_action.drag_progress.connect ((a, dx, dy)=>{
        a.x += dx;
        a.y += dy;
      });
    });
  }
  public ActorContainer.with_parent (Clutter.Actor parent, double width_mm, double height_mm) {
    parent.add_child (this);
    width = parent.width;
    height = parent.height;
    main_actor.x_ppm = x_ppm;
    main_actor.y_ppm = y_ppm;
    main_actor.height_mm = height_mm;
    main_actor.width_mm = width_mm;
  }
  // Virtual methods

  // Methods
  /**
   * Adds a new svg image
   */
  public GSvg.SVGElement add_svg (double x, double y, GSvg.Document svg) throws GLib.Error
    requires (svg.root_element != null)
  {
    var nsvg = main_doc.create_svg (null, null, null, null, null);
    nsvg.read_from_string (svg.root_element.write_string ());
    nsvg.x = new GsAnimatedLength ();
    nsvg.x.base_val = new GsLength ();
    nsvg.x.base_val.value = x;
    nsvg.y = new GsAnimatedLength ();
    nsvg.y.base_val = new GsLength ();
    nsvg.y.base_val.value = y;
    string id = "svg%d".printf (line_index++);
    var tl = main_element.svgs.get (id);
    while (tl != null) {
      id = "svg%d".printf (svg_index++);
      tl = main_element.svgs.get (id);
    }
    nsvg.id = id;
    main_g.append_child (nsvg);
    main_actor.drawing_area.invalidate ();
    return nsvg;
  }
  /**
   * Adds a new line element
   */
  public GSvg.LineElement add_line (double x1, double y1, double x2, double y2) throws GLib.Error {
    var l = main_element.create_line (x1.to_string ()+"mm",
                                      y1.to_string ()+"mm",
                                      x2.to_string ()+"mm",
                                      y2.to_string ()+"mm");
    string id = "line%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "line%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    l.id = id;
    l.stroke = "black";
    l.stroke_width = "1mm";
    main_g.append_child (l);
    main_actor.drawing_area.invalidate ();
    return l;
  }
  /**
   * Adds a new rect element
   */
  public GSvg.RectElement add_rect (double x,
                                    double y,
                                    double width,
                                    double height,
                                    double rx,
                                    double ry,
                                    string? style = null) throws GLib.Error
  {
    var r = main_element.create_rect (x.to_string ()+"mm",
                                      y.to_string ()+"mm",
                                      width.to_string ()+"mm",
                                      height.to_string ()+"mm",
                                      rx.to_string ()+"mm",
                                      ry.to_string ()+"mm",
                                      style);
    string id = "rect%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "rect%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    r.id = id;
    r.stroke = "black";
    r.stroke_width = "1mm";
    r.fill = "none";
    main_g.append_child (r);
    main_actor.drawing_area.invalidate ();
    return r;
  }
  /**
   * Adds a new circle element
   */
  public GSvg.CircleElement add_circle (double cx,
                                    double cy,
                                    double r) throws GLib.Error
  {
    var c = main_element.create_circle (cx.to_string ()+"mm",
                                      cy.to_string ()+"mm",
                                      r.to_string ()+"mm");
    string id = "circle%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "circle%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    c.id = id;
    c.stroke = "black";
    c.stroke_width = "1mm";
    c.fill = "none";
    main_g.append_child (c);
    main_actor.drawing_area.invalidate ();
    return c;
  }
  /**
   * Adds a new ellipse element
   */
  public GSvg.EllipseElement add_ellipse (double cx,
                                          double cy,
                                          double rx,
                                          double ry) throws GLib.Error
  {
    var c = main_element.create_ellipse (cx.to_string ()+"mm",
                                        cy.to_string ()+"mm",
                                        rx.to_string ()+"mm",
                                        ry.to_string ()+"mm");
    string id = "ellipse%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "ellipse%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    c.id = id;
    c.stroke = "black";
    c.stroke_width = "1mm";
    c.fill = "none";
    main_g.append_child (c);
    main_actor.drawing_area.invalidate ();
    return c;
  }
  /**
   * Adds a new polylline element
   */
  public GSvg.PolylineElement add_polyline (string points, GSvg.Length.Type units, string? transform, string? style=null) throws GLib.Error {
    var l = main_element.create_polyline (convert_points_to_units (points, units), style);
    string id = "polyline%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "polylline%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    l.id = id;
    l.stroke = "black";
    l.stroke_width = "1mm";
    l.fill = "none";
    if (transform != null)  {
      var t = new GsAnimatedTransformList ();
      t.value = transform;
      var gn = main_element.create_g ();
      gn.transform = t;
      gn.append_child (l);
      main_g.append_child (gn);
    } else {
      main_g.append_child (l);
    }
    main_actor.drawing_area.invalidate ();
    return l;
  }
  /**
   * Adds a new polygon element
   */
  public GSvg.PolygonElement add_polygon (string points, GSvg.Length.Type units, string? transform, string? style=null) throws GLib.Error {

    var l = main_element.create_polygon (convert_points_to_units (points, units), style);
    string id = "polygon%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "polygon%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    l.id = id;
    l.stroke = "black";
    l.stroke_width = "1mm";
    l.fill = "none";
    if (transform != null)  {
      var t = new GsAnimatedTransformList ();
      t.value = transform;
      var gn = main_element.create_g ();
      gn.transform = t;
      gn.append_child (l);
      main_g.append_child (gn);
    } else {
      main_g.append_child (l);
    }
    main_actor.drawing_area.invalidate ();
    return l;
  }
  /**
   * Adds a new text element.
   * @param text A text to show
   * @param font_family A text font family like "Sans"
   * @param font_size A measure of font size
   * @param xs If a single <coordinate> is provided, then the value represents the new absolute X coordinate for the current text position for rendering the glyphs that correspond to the first character within this element or any of its descendants.
   * @param ys The corresponding list of absolute Y coordinates for the glyphs corresponding to the characters within this element.
   * @param dys Shifts in the current text position along the x-axis for the characters within this element or any of its descendants.
   * @param dys Shifts in the current text position along the y-axis for the characters within this element or any of its descendants.
   * @param rotates The supplemental rotation about the current text position that will be applied to all of the glyphs corresponding to each character within this element.
   */
  public GSvg.TextElement add_text (string? text,
                                    string? font_family,
                                    string? font_size,
                                    string? xs,
                                    string? ys,
                                    string? dxs,
                                    string? dys,
                                    string? rotates) throws GLib.Error {
    var t = main_element.create_text (text, xs, ys, dxs, dys, rotates, null);
    string id = "text%d".printf (line_index++);
    var tl = main_element.lines.get (id);
    while (tl != null) {
      id = "text%d".printf (line_index++);
      tl = main_element.lines.get (id);
    }
    t.id = id;
    t.fill="black";
    t.font_family = font_family;
    t.font_size = font_size;
    main_g.append_child (t);
    main_actor.drawing_area.invalidate ();
    return t;
  }
  /**
   * Convert the units in given points to coordinates in the given units.
   *
   * Returned string is a representation of a {@link GSvg.AnimatedPointList}
   * converted to equivalent pixels in current units in the top most SVG element.
   */
  private string convert_points_to_units (string points, GSvg.Length.Type units) {
    var list = new GsPointList ();
    list.value = points;
    double multiplier = x_ppm;
    var m = new GsAnimatedLength ();
    m.value = "1mm";
    switch (units) {
      case GSvg.Length.Type.UNKNOWN:
      case GSvg.Length.Type.NUMBER:
      case GSvg.Length.Type.PX:
        multiplier *= 1.0;
      break;
      case GSvg.Length.Type.PERCENTAGE:
        multiplier *= 0.01;
      break;
      case GSvg.Length.Type.EMS:
        multiplier *= 0.01;
      break;
      case GSvg.Length.Type.EXS:
        if (main_g.font_size != null) {
          m.value = main_g.font_size;
        }
        multiplier *= m.base_val.value;
      break;
      case GSvg.Length.Type.CM:
        switch (main_element.width.base_val.unit_type) {
          case GSvg.Length.Type.MM:
            multiplier *= 10.0;
          break;
          case GSvg.Length.Type.CM:
            multiplier *= 1.0;
          break;
          case GSvg.Length.Type.IN:
            multiplier *= 2.54;
          break;
        }
      break;
      case GSvg.Length.Type.MM:
        switch (main_element.width.base_val.unit_type) {
          case GSvg.Length.Type.MM:
            multiplier *= 1.0;
          break;
          case GSvg.Length.Type.CM:
            multiplier *= 0.1;
          break;
          case GSvg.Length.Type.IN:
            multiplier *= 25.4;
          break;
        }
      break;
      case GSvg.Length.Type.IN:
        switch (main_element.width.base_val.unit_type) {
          case GSvg.Length.Type.MM:
            multiplier *= 25.4;
          break;
          case GSvg.Length.Type.CM:
            multiplier *= 2.54;
          break;
          case GSvg.Length.Type.IN:
            multiplier *= 1.0;
          break;
        }
      break;
      case GSvg.Length.Type.PT:
        multiplier *= 1.25;
      break;
      case GSvg.Length.Type.PC:
        multiplier *= 15;
      break;
    }
    for (int i = 0; i < list.number_of_items; i++) {
      try {
        var p = list.get_item (i);
        if (p == null) continue;
        p.x *= multiplier;
        p.y *= multiplier;
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    }
    return list.value;
  }
}