/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-svg-renderer-librsvg.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Rsvg;

public class GSvgtk.SvgRendererRsvg : Object, SvgRenderer {
  public GSvg.Document svg { get; set; }
  public string id { get; set; }
  public double x_ppm { get; set; }
  public double y_ppm { get; set; }
  construct {
    x_ppm = 4;
    y_ppm = 4;
  }
  public void render (Cairo.Context ctx) throws GLib.Error {
    if (svg == null) return;
    var rsvg = new Rsvg.Handle ();
    rsvg.set_dpi_x_y (x_ppm*25.4, y_ppm*25.4);
    rsvg.write (svg.write_string ().data);
    rsvg.close ();
    if (id != "" && id != null) {
      var e = svg.get_element_by_id (id);
      if (e != null) {
        rsvg.render_cairo_sub (ctx, id);
        return;
      }
    }
    rsvg.render_cairo (ctx);
  }
  public void render_sized (Cairo.Context ctx, double width, double height) throws GLib.Error { render (ctx); }
  public Gdk.Pixbuf? render_pixbuf () throws GLib.Error {
    if (svg == null) return null;
    var rsvg = new Rsvg.Handle ();
    rsvg.set_dpi_x_y (x_ppm*25.4, y_ppm*25.4);
    rsvg.write (svg.write_string ().data);
    rsvg.close ();
    return rsvg.get_pixbuf ();
  }
  public Gdk.Pixbuf? render_pixbuf_sized (double width, double height) throws GLib.Error { return render_pixbuf (); }
}
