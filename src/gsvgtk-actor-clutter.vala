/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-clutter.vala
 *
 * Copyright (C) 2017-2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Clutter;
using Cairo;

public class GSvgtk.ActorClutter : Clutter.Actor, GSvgtk.Positionable, GSvgtk.ActorResizable, GSvgtk.Actor {
  private double _x_ppm;
  private double _y_ppm;
  private double _width_mm;
  private double _heigth_mm;
  private GSvg.Document _svg;
  private GSvgtk.ActorCanvasClutter _canvas;
  private GSvgtk.SvgRenderer _renderer;
  private string default_image = """<?xml version="1.0"?>
<svg xmlns:svg="http://www.w3.org/2000/svg" width="400" height="400">
 <g font-family="Verdana" font-size="60">
   <text fill="blue" x="20" y="200">No <tspan font-weight="bold" fill="red">Image</tspan></text>
 </g>
 <rect fill="none" stroke="blue" stroke-width="5" x="2" y="2" width="395" height="395"/>
</svg>""";

  // ActorResizable implementation

  /**
   * Pixels per milimeter in the x axis
   */
  public double x_ppm {
    get { return _x_ppm; }
    set {
      _x_ppm = value;
      (this as Clutter.Actor).width = (float) (_x_ppm * _width_mm);
      renderer.x_ppm = _x_ppm;
      drawing_area.invalidate ();
    }
  }
  /**
   * Pixels per milimeter in the y axis
   */
  public double y_ppm {
    get { return _y_ppm; }
    set {
      _y_ppm = value;
      (this as Clutter.Actor).height = (float) (_y_ppm * _heigth_mm);
      renderer.y_ppm = _y_ppm;
      drawing_area.invalidate ();
    }
  }
  /**
   * Width in milimeters
   */
  public double width_mm {
    get { return _width_mm; }
    set {
      _width_mm = value;
      (this as Clutter.Actor).width = (float) (_x_ppm * _width_mm);
      drawing_area.invalidate ();
    }
  }
  /**
   * Height in milimeters
   */
  public double height_mm {
    get { return _heigth_mm; }
    set {
      _heigth_mm = value;
      (this as Clutter.Actor).height = (float) (_y_ppm * _heigth_mm);
      drawing_area.invalidate ();
    }
  }
  public float width_px { get { return width; } }
  public float height_px { get { return height; } }

  // GSvgtk.Actor implementation
  public GSvg.Document svg {
    get { return _svg; }
    set {
      _svg = value;
      if (_renderer == null) _renderer = new SvgRendererRsvg ();
      _renderer.svg = _svg;
      drawing_area.invalidate ();
    }
  }
  public SvgRenderer renderer {
    get { return _renderer; }
    set {
      _renderer = value;
      _renderer.x_ppm = x_ppm;
      _renderer.y_ppm = y_ppm;
      drawing_area.invalidate ();
    }
  }
  public GSvgtk.DrawingArea drawing_area { get { return _canvas; } }
  public GSvgtk.Editor editor {  get; internal set; }

  // Positionable
  public double x_mm {
    get { return x / x_ppm; }
    set { x = (float) (value * x_ppm); }
  }
  public double y_mm {
    get { return y / y_ppm; }
    set { y = (float) (value * y_ppm); }
  }

  construct {
    x = 0;
    y = 0;
    _x_ppm = 4.0;
    _y_ppm = 4.0;
    _width_mm = 100.0;
    _heigth_mm = 100.0;
    this.reactive = true;
    background_color = Clutter.Color.from_string ("#ffffff00");
    _svg = new GSvg.GsDocument ();
    try {
      _svg.read_from_string (default_image);
      _renderer = new SvgRendererRsvg (); // Librsvg for default
      _renderer.svg = _svg;
      _canvas = new ActorCanvasClutter (this);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }

  public void set_svg_string (string str) throws GLib.Error {
    if (svg == null) _svg = new GSvg.GsDocument ();
    _svg.read_from_string (str);
    renderer.svg = _svg;
  }

  // Virtual/Abstract Method

  public virtual bool draw_svg (Cairo.Context ctx, double width, double height) {
    try {
      if (renderer == null) return false;
      renderer.render_sized (ctx, width, height);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return true;
  }

  // Static methods
  public static double get_dpi (double width, GSvg.Length.Type units) {
    double m = 92;
    switch (units) {
      case GSvg.Length.Type.MM:
        m = width / 25.4;
        break;
      case GSvg.Length.Type.CM:
        m = width / 2.54;
        break;
    }
    return m;
  }

  /**
   * @param val Value to convert
   * @param type A GSvg.Length.Type to convert from
   * @param r Reference por GSvg.Length.Type.PERCENTAGE
   */
  public static float convert_svg_units_to_pixels (double val, GSvg.Length.Type type, double r = 1) {
    if (type == GSvg.Length.Type.UNKNOWN) return (float) val;
    Clutter.Units uc = Clutter.Units ();
    switch (type) {
      case  GSvg.Length.Type.PX:
      case  GSvg.Length.Type.NUMBER:
      case  GSvg.Length.Type.EXS:
      case  GSvg.Length.Type.PT:
      case  GSvg.Length.Type.PC:
        uc = Clutter.Units.from_pixels ((int) val);
        break;
      case  GSvg.Length.Type.EMS:
        uc = Clutter.Units.from_em ((float) val);
        break;
      case  GSvg.Length.Type.MM:
        uc = Clutter.Units.from_mm ((float) val);
        break;
      case  GSvg.Length.Type.CM:
        uc = Clutter.Units.from_cm ((float) val);
        break;
      case  GSvg.Length.Type.IN:
        uc = Clutter.Units.from_mm ((float) (val/25.4));
        break;
      case GSvg.Length.Type.PERCENTAGE:
        // FIXME: También se deben tomar en cuenta las unidades del padre de referencia
        uc = Clutter.Units.from_pixels ((int) (val * r));
        break;
    }
    uc.to_pixels ();
    return uc.get_unit_value ();
  }
}
