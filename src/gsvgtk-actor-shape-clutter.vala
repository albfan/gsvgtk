/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-shape-clutter.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;
using Clutter;
using Cairo;

public abstract class GSvgtk.ActorShapeClutter : GSvgtk.ActorClutter, GSvgtk.ActorShape {
  // Properties
  public string id { get; internal set; }
  public GSvg.Element element { get; internal set; }
  public GSvg.Document fake_doc { get; internal set; }
  public GSvg.Element mirror_element { get; internal set; }
  public GSvg.Element mirror_gelement { get; internal set; }
  // Constructor
  construct {
    fake_doc = new GSvg.GsDocument ();
    try {
      fake_doc.add_svg (null, null, "20mm", "20mm");
      var s = fake_doc.root_element.create_circle ("10mm", "10mm", "18mm");
      s.fill = "yellow";
      s.stroke = "red";
      s.stroke_width = "3mm";
      s.id = "circle";
      fake_doc.root_element.append_child (s);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }
}
