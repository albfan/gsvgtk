/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-editor-clutter.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public class GSvgtk.EditorClutter : Object, GSvgtk.Editor {
  private Clutter.DragAction drag_action = null;
  private bool dragging = false;
  private GSvg.Point move_begin = new GsPoint ();
  private GSvg.Point move_end = new GsPoint ();
  private Clutter.Text text;

  public GSvgtk.Changes changes { get; internal set; }
  public GSvgtk.Actor actor  { get; internal set; }
  public GSvg.PointList movedto { get; internal set; }
  public GSvg.Element modifications { get; internal set; }

  construct {
    movedto = new GsPointList ();
  }

  public EditorClutter (GSvgtk.Actor actor) {
    // FIXME: Initialize changes
    if (!(actor is Clutter.Actor)) {
      warning ("Invalid attempt to use a non-Clutter.Actor for EditorClutter");
      return;
    }
    this.actor = actor;
    text = new Clutter.Text.full ("Sans","0.0mm,0.0mm", Clutter.Color.from_string ("#d3d7cfff"));
    (actor as Clutter.Actor).add_child (text);
    text.x = 0.0f;
    text.y = -20.0f;
    GSvg.Point p = current_point ();
    string to = "%gmm,%gmm".printf (p.x, p.y);
    text.buffer.set_text (to, to.length);
    text.visible = false;
    drag_action = (actor as Clutter.Actor).get_action ("drag-action") as Clutter.DragAction;
    if (drag_action == null) {
      warning ("Actor doesn't expose a DragAction");
      return;
    }
    drag_action.drag_begin.connect ((a, x, y, mod)=>{
      try {
        if (movedto.number_of_items > 0) {
          p = movedto.get_item (movedto.number_of_items - 1);
        } else {
          p = current_point ();
        }
        dragging = true;
        string st = "%gmm,%gmm".printf (p.x, p.y);
        text.buffer.set_text (st, st.length);
        float xi = (actor as Clutter.Actor).x;
        float yi = (actor as Clutter.Actor).y;
        move_begin.x = xi;
        move_begin.y = yi;
        if (xi == 0) {
          text.x += 25.0f;
        } else if (xi < 0) {
          text.x = 0;
        }
        if (yi == 0) {
          text.y += 25.0f;
        } else if (yi < 0) {
          text.y = 0;
        }
        text.visible = true;
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    });
    drag_action.drag_progress.connect ((a, dx, dy)=>{
      dragging = true;
      float x = 0.0f;
      float y = 0.0f;
      (actor as Clutter.Actor).get_position (out x, out y);
      double px = p.x + (x - move_begin.x) / actor.x_ppm;
      double py = p.y + (y - move_begin.y) / actor.y_ppm;
      string t = "%gmm,%gmm".printf (px, py);
      float mx = (actor as Clutter.Actor).get_stage ().width;
      float my = (actor as Clutter.Actor).get_stage ().height;
      Pango.Rectangle iext = Pango.Rectangle ();
      Pango.Rectangle lext = Pango.Rectangle ();
      text.get_layout ().get_pixel_extents (out iext, out lext);
      if (x == 0) {
        text.x += 25.0f;
      } else if (x < 0) {
        text.x = -x;
      } else if (x > 0 && x + iext.width < mx) {
        text.x = 0;
      } else if ((x + iext.width) > mx) {
        text.x = -x + mx - iext.width;
      }
      if (y == 0) {
        text.y += 25.0f;
      } else if (y < 0) {
        text.y = -y;
      } else if (y > 0 && y + iext.height < my) {
        text.y = -20.0f;
      } else if (y + iext.height > my) {
        text.y = - y +my - iext.height;
      }
      text.buffer.set_text (t, t.length);
      return true;
    });
    drag_action.drag_end.connect ((a, x, y, mod)=>{
      dragging = false;
      move_end.x = ((actor as Clutter.Actor).x - move_begin.x) / actor.x_ppm;
      move_end.y = ((actor as Clutter.Actor).y - move_begin.y) / actor.y_ppm;
      try {
        movedto.append_item (move_end);
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
      text.visible = false;
    });
  }
  public void undo () {
    if (movedto.number_of_items > 0) {
      try {
        var p = movedto.get_item (movedto.number_of_items - 1);
        actor.x_mm -= p.x;
        actor.y_mm -= p.y;
        movedto.remove_item (movedto.number_of_items - 1);
      } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    }
  }
}
